// * Task1 
const product = {
    name: "Planner",
    oldPrice: 300,
    newPrise: 155,
    discount: true,
    getDiscountAmount() {
        return product.oldPrice - product.newPrise;
    }
}
console.log(product.getDiscountAmount());

// *Task 2

function greeting(userName, userAge) {
    alert(`Hi, my name is ${userName} and I am ${userAge} .`);
}

let userName = prompt("Enter your name , please.");
let userAge = prompt("Enter your age , please");

while (isNaN(userAge) || userAge <= 4) {
    alert("It's not your age. Try again");
    userAge = prompt("Enter your age , please");
}

greeting(userName, userAge);

// *Task 3

// // створила функцію
// function recursClonObj(obj) {
//     // створила пустий  об'єкт ,у який треба передавати ключі
//     let copyObj = {};
//     // перевірка на тип 
//     if (typeof obj === 'object' && obj !== null) {
//         // перебирання об'єкту циклом фор
//         for (let key in obj) {
//             // якщо у об'єкта є ключ , то...
//             if (hasOwnProperty.call(obj, key)) {
//                 // copyObj[key] надаємо значення ключів для копії
//                 copyObj[key] = recursClonObj(obj[key]);
//             }
//         }
//     }
//     // повертаємо об'єкт
//     return obj;
// }

// тест
// створюємо об'єкт
const user = {
    name: "Vasya",
    age: 13,
    pet: {
        name: "Pushin"
    }
};

function recursClonObj(user) {
    let copyUser = {};
    if (typeof user === 'object' && user !== null) {
        for (let key in user) {
            if (hasOwnProperty.call(user, key)) {
                copyUser[key] = recursClonObj(user[key]);
            }
        }
    }
    return user;
}

console.log(recursClonObj(user));
// тут видало true , отже все вірно ? я очікувала при тесті побачити тут фолс ... 
console.log(user === recursClonObj(user)); 